########################
# README
########################

Provides:
	- Allows masquerading and packet forwarding from specific hosts
	- Uses MAC addresses for finite access control

Format (reads from /etc/firewall/whitelist - one per line):

	- {MAC}:MA:CA:DD:RE:SS{MAC}:Anything here /*Something descriptive*/
	- Ex: {MAC}:01:23:45:67:89:ab{MAC}:Netbook
