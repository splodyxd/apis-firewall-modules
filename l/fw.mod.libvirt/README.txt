########################
# README
########################

Provides:
	- Bridge (br0) forwarding rules
	- DNS/DHCP rules
	- Libvirt rules
	- Allow VM internet access
	- VNC rules
